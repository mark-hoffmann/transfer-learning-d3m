#!/bin/bash -e

# Assumption is that this repository is cloned into "common-primitives" directory
# which is a sibling of "primitives_repo" directory.

for PRIMITIVE in $(./list_primitives.py --python); do
  echo $PRIMITIVE
  python3 -m d3m.index describe -i 4 $PRIMITIVE > primitive.json
  pushd ../primitives_repo
  ./add.py ../transfer-learning/primitive.json
  popd
done
