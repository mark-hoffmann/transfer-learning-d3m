from __future__ import division

from .models import *
from .utils.utils import *
from .utils.d3mdatasets import *
from .utils.parse_config import *

import os
import sys
import time
import datetime
import argparse
import requests
from fastprogress.fastprogress import master_bar, progress_bar

import torch
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision import transforms
from torch.autograd import Variable
import torch.optim as optim

import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.ticker import NullLocator

import os
import typing
from typing import Dict, List

#import common_primitives.utils as common_utils
import numpy
import pandas as pd
import tqdm

# Custom import commands if any
from pathlib import Path

from d3m import utils
from d3m.container import DataFrame as d3m_dataframe
from d3m.container.numpy import ndarray as d3m_ndarray
from d3m.metadata import hyperparams, params, base as metadata_base
from d3m.primitive_interfaces.base import CallResult, DockerContainer, ContinueFitMixin
from d3m.primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase
#from jpl_common_primitives import __version__
__version__ = '2018.07.16'

Inputs = d3m_dataframe
Outputs = d3m_dataframe



class Params(params.Params):
    pass


class Hyperparams(hyperparams.Hyperparams):

    learning_rate = hyperparams.Bounded(
        lower=0.00001,
        upper=0.1,
        default=0.001,
        description="learning rate",
        semantic_types=['https://metadata.datadrivendiscovery.org/types/MetafeatureParameter']
    )

    momentum = hyperparams.Bounded(
        lower=0.5,
        upper=1,
        default=0.9,
        description="momentum",
        semantic_types=['https://metadata.datadrivendiscovery.org/types/MetafeatureParameter']
    )

    decay = hyperparams.Bounded(
        lower=0.00005,
        upper=0.05,
        default=0.0005,
        description="decay",
        semantic_types=['https://metadata.datadrivendiscovery.org/types/MetafeatureParameter']
    )

    batch_size = hyperparams.UniformInt(
        lower=1,
        upper=500,
        default=16,
        description="batch size",
        semantic_types=['https://metadata.datadrivendiscovery.org/types/MetafeatureParameter']
    )

    conf_thres = hyperparams.Bounded(
        lower=0.5,
        upper=0.95,
        default=0.8,
        description="confidence threshhold",
        semantic_types=['https://metadata.datadrivendiscovery.org/types/MetafeatureParameter']
    )

    nms_thres = hyperparams.Bounded(
        lower=0.2,
        upper=0.7,
        default=0.4,
        description="nms thresh",
        semantic_types=['https://metadata.datadrivendiscovery.org/types/MetafeatureParameter']
    )

    img_size = hyperparams.UniformInt(
        lower=256,
        upper=512,
        default=416,
        description="size of each image dimension",
        semantic_types=['https://metadata.datadrivendiscovery.org/types/MetafeatureParameter']
    )

    checkpoint_interval = hyperparams.UniformInt(
        lower=1,
        upper=3,
        default=1,
        description="internal between saving model weights",
        semantic_types=['https://metadata.datadrivendiscovery.org/types/MetafeatureParameter']
    )

    interested_class = hyperparams.Enumeration(
        values=["person","bicycle","car","motorbike","aeroplace","bus","train","truck","boat","traffic light","fire hydrant",
                "stop sign","parking meter","bench","bird","cat","dog","horse","sheep","cow","elephant","bear","zebra",
                "giraffe","backpack","umbrella","handbag","tie","suitcase","frisbee","skis","snowboard","sports ball","kite",
                "baseball bat", "baseball glove", "skateboard", "surfboard","tennis racket","bottle","wine glass","cup",
                "fork","knife","spoon","bowl","banana","apple","sandwich","orange","broccoli","carrot","hot dog","pizza",
                "donut","cake","chair","sofa","pottedplant","bed","diningtable","toilet","tvmonitor","laptop","mouse",
                "remote","keyboard","cell phone","microwave","oven","toaster","sink","refrigerator","book","clock","vase",
                "scissors","teddy bear","hair drier","toothbrush"],
        default="person",
        description="Class of interest we will be feeding for fine tuning and using for prediction.",
        semantic_types=['https://metadata.datadrivendiscovery.org/types/MetafeatureParameter']
    )

    pretrained_network = hyperparams.Enumeration(
        values=["No","coco"],
        default="coco",
        description="If we want to use pretrained weights for our network",
        semantic_types=['https://metadata.datadrivendiscovery.org/types/MetafeatureParameter']
    )





class YOLOv3(SupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams],
             ContinueFitMixin[Inputs, Outputs, Params, Hyperparams]):

    metadata = metadata_base.PrimitiveMetadata({
        "algorithm_types": ['STOCHASTIC_NEURAL_NETWORK'],
        "name": "YOLOv3",
        "primitive_family": metadata_base.PrimitiveFamily.OBJECT_DETECTION,
        "python_path": "d3m.primitives.object_detection.YOLOv3.Transfer_Learning",
        "source": {'name': 'JPL', 'contact': 'mailto:mark.k.hoffmann@jpl.nasa.gov', 'uris': ['https://gitlab.com/mark-hoffmann/transfer-learning-d3m']},
        "version": __version__,
        "id": "b9c0c3dd-534f-4bf2-8381-4b2b1e931bcf",
        "installation": [{'type': metadata_base.PrimitiveInstallationType.PIP,
            'package_uri': 'git+https://gitlab.com/mark-hoffmann/transfer-learning-d3m.git@{git_commit}#egg=transfer-learning-d3m'.format(
                git_commit=utils.current_git_commit(os.path.dirname(__file__)),
            ),
            }]
    })

    def __init__(self, *,
                 hyperparams: Hyperparams,
                 random_seed: int = 0,
                 docker_containers: Dict[str, DockerContainer] = None,
                 _verbose: int = 0
                 ) -> None:

        super().__init__(hyperparams=hyperparams, random_seed=random_seed, docker_containers=docker_containers)
        self.hyperparams = hyperparams
        self.random_seed = random_seed
        self._fitted = False

        #Config params on Instantiation
        self._model_config_path = f"{os.path.abspath(os.path.dirname(__file__))}/config/yolov3.cfg"
        self._class_path = f"{os.path.abspath(os.path.dirname(__file__))}/data/coco.names"
        self._cuda = torch.cuda.is_available()
        self._n_cpu = 0

        self._classes = load_classes(self._class_path)

        # Get hyper parameters
        self._learning_rate = self.hyperparams['learning_rate']
        self._momentum = self.hyperparams['momentum']
        self._decay = self.hyperparams['decay']
        self._burn_in = 1000



        # Initiate model, if we don't have weights downloaded, do so
        self._model = Darknet(self._model_config_path)
        if self.hyperparams['pretrained_network'] == "coco":
            self._weights_path = f"{os.path.abspath(os.path.dirname(__file__))}/weights/yolov3.weights"
            if not Path(self._weights_path).exists():
                print("Downloading YOLO pretrained weights...")
                s = requests.Session()
                s.mount('http://',requests.adapters.HTTPAdapter(max_retries=3))
                u = s.get("https://pjreddie.com/media/files/yolov3.weights", stream=True, timeout=None)
                try: file_size = int(u.headers["Content-Length"])
                except: show_progress = False

                with open(self._weights_path, 'wb') as f:
                    nbytes = 0
                    pbar = progress_bar(range(file_size), auto_update=False, leave=False, parent=None)
                    try:
                        for chunk in u.iter_content(chunk_size=1024*1024):
                            nbytes += len(chunk)
                            pbar.update(nbytes)
                            f.write(chunk)
                    except Exception as e:
                        print("Exception Downloading weights...")
                        print(e)

            self._model.load_weights(self._weights_path)
        else:
            self._model.apply(weights_init_normal)  #If we want to initialize our weights to 0 and train from scratch -- Rarely if ever what we want to do...

        if self._cuda:
            if torch.cuda.device_count() > 1:
                print(f"Using {torch.cuda.device_count()} GPUs")
                self._model = nn.DataParallel(self._model)

            self._model = self._model.cuda()

        self._Tensor = torch.cuda.FloatTensor if self._cuda else torch.FloatTensor




    def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:
        """
        Grabs a new batch from our data directory. Iterates through our data loader

        """

        # Set data loader configuration for D3M dataset
        # If we are running for test purposes on coco, we need a slightly different data loader, which is the else case

        if type(inputs) == d3m_dataframe:
            self._coco = False
            _trn_path = str(Path(inputs.loc[0]["full_path"]).parent.parent)
            self._train_path = _trn_path
            self._dataloader = torch.utils.data.DataLoader(
                D3MDataset(self._train_path, class_index=self._classes.index(self.hyperparams['interested_class'])),
                batch_size=self.hyperparams['batch_size'], shuffle=False, num_workers=self._n_cpu)
        else:
            self._coco = True
            #data_config = parse_data_config(self._data_config_path)
            #self._train_path = data_config['train']
            self._train_path = "data/coco/trainvalno5k.txt"
            self._dataloader = torch.utils.data.DataLoader(
                ListDataset(self._train_path),
                batch_size=self.hyperparams['batch_size'], shuffle=False, num_workers=self._n_cpu)


        return


    def fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:


        optimizer = optim.SGD(self._model.parameters(), lr=self._learning_rate, momentum=self._momentum, dampening=0, weight_decay=self._decay)

        self._model.train()

        for epoch in range(iterations):
            for batch_i, (_, imgs, targets) in enumerate(self._dataloader):
                imgs = Variable(imgs.type(self._Tensor))
                targets = Variable(targets.type(self._Tensor), requires_grad=False)

                optimizer.zero_grad()

                loss = self._model(imgs, targets)

                loss.sum().backward()
                optimizer.step()

                if torch.cuda.device_count() > 1:
                    losses = self._model.module.losses
                else:
                    losses = self._model.losses
                print('[Epoch %d/%d, Batch %d/%d] [Losses: x %f, y %f, w %f, h %f, conf %f, cls %f, total %f, recall: %.5f]' %
                                            (epoch, iterations, batch_i, len(self._dataloader),
                                            losses['x'], losses['y'], losses['w'],
                                            losses['h'], losses['conf'], losses['cls'],
                                            loss.sum().item(), losses['recall']))

                if torch.cuda.device_count() > 1:
                    self._model.module.seen += imgs.size(0)
                else:
                    self._model.seen += imgs.size(0)

            """
            if epoch % self.hyperparams['checkpoint_interval'] == 0:
                if torch.cuda.device_count() > 1:
                    self._model.module.save_weights('checkpoints/epoch_%d.weights' % (epoch))
                else:
                    self._model.save_weights('checkpoints/epoch_%d.weights' % (epoch))
            """

        return(CallResult(None))

    def continue_fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:

        self.fit(iterations = iterations)

        return CallResult(None)

    def _calculate_mAP(self, *, inputs: Inputs, _prediction_classes: list = ["all"]) -> None:

        self._model.eval()

        output_classes = self._classes if _prediction_classes == ["all"] else _prediction_classes

        _trn_path = str(Path(inputs.loc[0]["full_path"]).parent.parent)
        self._train_path = _trn_path
        self._dataloader = torch.utils.data.DataLoader(
            D3MDataset(self._train_path, mAP_score=True),
            batch_size=self.hyperparams['batch_size'], shuffle=False, num_workers=self._n_cpu)


        Tensor = torch.cuda.FloatTensor if self._cuda else torch.FloatTensor

        print("Compute mAP...")

        all_detections = []
        all_annotations = []

        for batch_i, (_, imgs, targets) in enumerate(tqdm.tqdm(self._dataloader, desc="Detecting objects")):

            imgs = Variable(imgs.type(Tensor))

            with torch.no_grad():
                outputs = self._model(imgs)
                outputs = non_max_suppression(outputs, 80, conf_thres=self.hyperparams['conf_thres'], nms_thres=self.hyperparams['nms_thres'])

            for output, annotations in zip(outputs, targets):

                all_detections.append([np.array([]) for _ in range(len(output_classes))])
                if output is not None:
                    # Get predicted boxes, confidence scores and labels
                    pred_boxes = output[:, :5].cpu().numpy()
                    scores = output[:, 4].cpu().numpy()
                    pred_labels = output[:, -1].cpu().numpy()

                    # Order by confidence
                    sort_i = np.argsort(scores)
                    pred_labels = pred_labels[sort_i]
                    pred_boxes = pred_boxes[sort_i]

                    for label in range(len(output_classes)):
                        all_detections[-1][label] = pred_boxes[pred_labels == label]

                all_annotations.append([np.array([]) for _ in range(len(output_classes))])
                if any(annotations[:, -1] > 0):

                    annotation_labels = annotations[annotations[:, -1] > 0, 0].numpy()
                    _annotation_boxes = annotations[annotations[:, -1] > 0, 1:]

                    # Reformat to x1, y1, x2, y2 and rescale to image dimensions
                    annotation_boxes = np.empty_like(_annotation_boxes)
                    annotation_boxes[:, 0] = _annotation_boxes[:, 0] - _annotation_boxes[:, 2] / 2
                    annotation_boxes[:, 1] = _annotation_boxes[:, 1] - _annotation_boxes[:, 3] / 2
                    annotation_boxes[:, 2] = _annotation_boxes[:, 0] + _annotation_boxes[:, 2] / 2
                    annotation_boxes[:, 3] = _annotation_boxes[:, 1] + _annotation_boxes[:, 3] / 2
                    annotation_boxes *= self.hyperparams["img_size"]

                    for label in range(len(output_classes)):
                        all_annotations[-1][label] = annotation_boxes[annotation_labels == label, :]

        average_precisions = {}
        for label in range(len(output_classes)):
            true_positives = []
            scores = []
            num_annotations = 0

            for i in tqdm.tqdm(range(len(all_annotations)), desc=f"Computing AP for class '{label}'"):
                detections = all_detections[i][label]
                annotations = all_annotations[i][label]

                num_annotations += annotations.shape[0]
                detected_annotations = []

                for *bbox, score in detections:
                    scores.append(score)

                    if annotations.shape[0] == 0:
                        true_positives.append(0)
                        continue

                    overlaps = bbox_iou_numpy(np.expand_dims(bbox, axis=0), annotations)
                    assigned_annotation = np.argmax(overlaps, axis=1)
                    max_overlap = overlaps[0, assigned_annotation]

                    iou_thres = 0.5
                    if max_overlap >= iou_thres and assigned_annotation not in detected_annotations:
                        true_positives.append(1)
                        detected_annotations.append(assigned_annotation)
                    else:
                        true_positives.append(0)

            # no annotations -> AP for this class is 0
            if num_annotations == 0:
                average_precisions[label] = 0
                continue

            true_positives = np.array(true_positives)
            false_positives = np.ones_like(true_positives) - true_positives
            # sort by score
            indices = np.argsort(-np.array(scores))
            false_positives = false_positives[indices]
            true_positives = true_positives[indices]

            # compute false positives and true positives
            false_positives = np.cumsum(false_positives)
            true_positives = np.cumsum(true_positives)

            # compute recall and precision
            recall = true_positives / num_annotations
            precision = true_positives / np.maximum(true_positives + false_positives, np.finfo(np.float64).eps)

            # compute average precision
            average_precision = compute_ap(recall, precision)
            average_precisions[label] = average_precision

        print("Average Precisions:")
        for c, ap in average_precisions.items():
            print(f"+ Class '{c}' - AP: {ap}")

        mAP = np.mean(list(average_precisions.values()))
        print(f"mAP: {mAP}")
        return



    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:

        #self._model.load_weights(self._weights_path) #Already loaded from constructor
        self._model.eval()

        if type(inputs) != str:
            _image_folder = str(Path(inputs.loc[0]["full_path"]).parent)
        else:
            _image_folder = inputs

        eval_dataloader = DataLoader(ImageFolder(_image_folder, img_size=self.hyperparams['img_size']),
                                batch_size=20, shuffle=False, num_workers=self._n_cpu)


        imgs = []           # Stores image paths
        img_detections = [] # Stores detections for each image index
        # Doing checking to make sure we have valid prediction classes

        #output_classes = self._classes if _prediction_classes == ["all"] else _prediction_classes
        output_classes = [self.hyperparams['interested_class']]

        print ('\nPerforming object detection:')
        prev_time = time.time()
        for batch_i, (img_paths, input_imgs) in enumerate(eval_dataloader):
            # Configure input
            input_imgs = Variable(input_imgs.type(self._Tensor))

            # Get detections
            with torch.no_grad():
                detections = self._model(input_imgs)
                #print("DETECTIONS")
                #print(detections.shape)
                #print(detections)
                detections = non_max_suppression(detections, 80, conf_thres=self.hyperparams['conf_thres'], nms_thres=self.hyperparams['nms_thres'])


            # Log progress
            current_time = time.time()
            inference_time = datetime.timedelta(seconds=current_time - prev_time)
            prev_time = current_time
            print ('\t+ Batch %d, Inference Time: %s' % (batch_i, inference_time))

            # Save image and detections
            imgs.extend(img_paths)
            img_detections.extend(detections)

        # Bounding-box colors
        cmap = plt.get_cmap('tab20b')
        colors = [cmap(i) for i in np.linspace(0, 1, 20)]

        print ('\nImages:')

        output_df = pd.DataFrame(columns=["d3mIndex","image","bounding_box","confidence"])

        for img_i, (path, detections) in enumerate(zip(imgs, img_detections)):

            print ("(%d) Image: '%s'" % (img_i, path))

            img = np.array(Image.open(path))
            """
            if _draw_outputs:
                # Create plot
                plt.figure()
                fig, ax = plt.subplots(1)
                ax.imshow(img)
            """
            # The amount of padding that was added
            pad_x = max(img.shape[0] - img.shape[1], 0) * (self.hyperparams['img_size'] / max(img.shape))
            pad_y = max(img.shape[1] - img.shape[0], 0) * (self.hyperparams['img_size'] / max(img.shape))
            # Image height and width after padding is removed
            unpad_h = self.hyperparams['img_size'] - pad_y
            unpad_w = self.hyperparams['img_size'] - pad_x


            # Draw bounding boxes and labels of detections
            if detections is not None:
                """
                if _draw_outputs:
                    unique_labels = detections[:, -1].cpu().unique()
                    n_cls_preds = len(unique_labels)
                    bbox_colors = random.sample(colors, n_cls_preds)
                """
                for x1, y1, x2, y2, conf, cls_conf, cls_pred in detections:
                    if self._classes[int(cls_pred)] in output_classes:
                        print ('\t+ Label: %s, Conf: %.5f' % (self._classes[int(cls_pred)], cls_conf.item()))

                        # Rescale coordinates to original dimensions
                        box_h = ((y2 - y1) / unpad_h) * img.shape[0]
                        box_w = ((x2 - x1) / unpad_w) * img.shape[1]
                        y1 = ((y1 - pad_y // 2) / unpad_h) * img.shape[0]
                        x1 = ((x1 - pad_x // 2) / unpad_w) * img.shape[1]

                        x2 = x1 + box_w
                        y2 = y1 + box_h

                        _path = path.split("/")[-1]
                        bb = str(round(x1.item(),1)) + "," + str(round(y1.item(),1)) + "," + str(round(x2.item(),1)) + ","  + str(round(y2.item(),1))

                        output_df = output_df.append({"image": _path, "bounding_box": bb, "confidence": round(cls_conf.item(),2)}, ignore_index=True)
                        """
                        if _draw_outputs:
                            color = bbox_colors[int(np.where(unique_labels == int(cls_pred))[0])]
                            # Create a Rectangle patch
                            bbox = patches.Rectangle((x1, y1), box_w, box_h, linewidth=2,
                                                    edgecolor=color,
                                                    facecolor='none')
                            # Add the bbox to the plot
                            ax.add_patch(bbox)
                            # Add label
                            plt.text(x1, y1, s=self._classes[int(cls_pred)], color='white', verticalalignment='top',
                                    bbox={'color': color, 'pad': 0})
                        """
                output_df["d3mIndex"] = range(0,len(output_df))
                #Path("output").mkdir(exist_ok = True)
                #output_df.to_csv('output/predictions.csv', index=False)

            # Save generated image with detections
            """
            if _draw_outputs:
                plt.axis('off')
                plt.gca().xaxis.set_major_locator(NullLocator())
                plt.gca().yaxis.set_major_locator(NullLocator())
                plt.savefig('output/%d.png' % (img_i), bbox_inches='tight', pad_inches=0.0)
                plt.close()
            """

        return(CallResult(output_df))


    def get_params(self) -> Params:
        pass

    def set_params(self, *, params: Params) -> None:
        pass























YOLOv3.__doc__ = ""
